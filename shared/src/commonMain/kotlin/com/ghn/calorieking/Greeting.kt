package com.ghn.calorieking

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}